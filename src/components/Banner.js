import Button from "react-bootstrap/Button";
/*Bootstrap Grid System*/
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import {Link} from 'react-router-dom';

import ProductCard from '../components/ProductCard';
import {Fragment, useEffect, useState} from 'react';

export default function Banner(){

	const [product, setProduct] = useState([]);
		
		useEffect(()=>{
			fetch(`${process.env.REACT_APP_URL}/products/allActiveProducts`)
			.then(response => response.json())
			.then(data =>{
				// console.log(data);

				setProduct(data.map(product =>{
			return(
				<Fragment>
				<ProductCard  key = {product._id} prop = {product}/>
				</Fragment>
				)
		}))
			})
		},[])

		return(
			<Fragment>
			<h1 className = "mt-3 fw-bold text-center">Featured Products:</h1>
			{product}
			</Fragment>
			)

}