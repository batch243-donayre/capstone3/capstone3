import ProductCard from '../components/ProductCard';
import {Fragment, useEffect, useState} from 'react';


export default function Products(){


const [product, setProduct] = useState([]);
	
	useEffect(()=>{
		fetch(`${process.env.REACT_APP_URL}/products/allActiveProducts`)
		.then(response => response.json())
		.then(data =>{
			// console.log(data);

			setProduct(data.map(product =>{
		return(
			<Fragment>
			<ProductCard  key = {product._id} prop = {product}/>
			</Fragment>
			)
	}))
		})
	},[])

	return(
		<Fragment>
		<h1 className = "mt-3 fw-bold text-center">Shop All Products:</h1>
		{product}
		</Fragment>
		)
}