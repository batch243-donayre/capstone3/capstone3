
import {Container} from 'react-bootstrap';
import Banner from '../components/Banner';
import '../App.css';



export default function Home(){
	
	return(
		<Container>
		  <Banner/>
		</Container>
	)
}