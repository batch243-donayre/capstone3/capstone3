import{useParams} from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {Container, Row, Col} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import{useNavigate} from 'react-router-dom';


export default function UpdateProducts(){
	const {productId} = useParams();

	const [productName, setProductName]  = useState ('');
	const [description, setDescription]  = useState ('');
	const [price, setPrice] = useState ('');


	useEffect(()=>{
		fetch(`${process.env.REACT_APP_URL}/products/getSpecificProduct/${productId}`)
		.then(response => response.json())
		.then(data =>{
			console.log(data);

			setProductName(data.productName);
			setDescription(data.description);
			setPrice(data.price);

		}).catch((err) => {
			console.log(err.message);
		})
	},[productId])


	const navigate = useNavigate();
	const{user,setUser} = useContext(UserContext);

	const update = (e) => {
	e.preventDefault();
		console.log(productName);

	fetch(`${process.env.REACT_APP_URL}/products/updateProduct/${productId}`,{
		method: "PUT",
		headers: {
			"Content-Type" : "application/json",
			Authorization: `Bearer ${localStorage.getItem('token')}`
		},
		body : JSON.stringify({
			productName : productName,
			description: description,
			price: price,
		})

	})
		.then(response => response.json())
		.then(data => {
			if(data){
				console.log(data)
				Swal.fire({
					title: "Updated Successfully!",
					icon: "success",
					// text: ""
				})
				navigate("/dashboard");
			}else{
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Try Again!"
				})
			}
		}).catch(err =>{
			console.log(err);
		})
}


		return (
			<Container>
			<Row>
				<Col className = "col-md-4 col-8 offset-md-4 offset-2">
					<Form  className = 'p-3' onSubmit = {update}>
					{/*onSubmit = {addProduct}*/}

						<h4 className="mb-3 mt-4">Updating Products Details:</h4>
						<Form.Group className="mb-3" controlId="productname">
						    <Form.Label className= "fw-bold">Product Name:</Form.Label>
						    <Form.Control 
						    type = "text"
						    placeholder="Enter Product Name" 
						    value = {productName}
						    onChange = {event => setProductName(event.target.value)}
					    	required/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="description">
						    <Form.Label className= "fw-bold">Description:</Form.Label>
						    <Form.Control 
						    type = "text"
						    placeholder="Enter Product description" 
						    value = {description}
						    onChange = {event => setDescription(event.target.value)}
					    	required
					    	className = "descriptionBox"/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="price">
						    <Form.Label className= "fw-bold">Price:</Form.Label>
						    <Form.Control 
						    type = "number"
						    placeholder="Enter Product Price" 
						    value = {price}
						    onChange = {event => setPrice(event.target.value)}
					    	required/>
						</Form.Group>
					  
					  <Button  variant="success" type="submit" className = "btnCreateProd">
					    Save Update
					  </Button>

					  <Button as = {Link} to = "/dashboard" variant="danger" type="submit" className = "btnCreateProd">
					    Back
					  </Button>

					</Form>
				</Col>

			</Row>
		    </Container>
		);
}